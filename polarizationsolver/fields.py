#!/usr/bin/env python3
from __future__ import division
import numpy as np
from . import tensors
from math import factorial
from string import ascii_lowercase as abc
from string import ascii_uppercase as ABC


def field(Rab, multipole_rank, multipoles, field_rank):
    """
    Calculates the field (derivatives) due to a set of multipoles in a set of points

    Rab: distance ndarrray
        shape is (nmult,...,3) so we can work with both a single distance vector (nmult,3), a vector of distance vectors (nmult,N,3) and so on
    multipole_rank: rank of the multipole
    multipoles: ndarray, cartesian multipole moments of the same rank, shape (number_of_multipoles) + (3)*multipole_rank
        0-> (number_of_multipoles,)    charge
        1-> (number_of_multipoles,3)   dipole
        2-> (number_of_multipoles,3,3) quadrupole
        ...
    field_rank: rank of output field
        0->  potential (negative of, depending on definitions)
        1->  field
        2->  field gradient
        ...
    """
    tensor_rank = field_rank + multipole_rank
    multipole_additional_rank = len(multipoles.shape) - multipole_rank
    Rab_additional_rank = len(Rab.shape) - multipole_additional_rank - 1

    Tn = tensors.T[tensor_rank](Rab)
    factor = -1.0 * (-1)**multipole_rank / factorial(multipole_rank)
    # Tn:multipoles->out
    # abc... for multipole/field contraction
    # ABC... for additional ranks
    A = ABC[0:multipole_additional_rank + Rab_additional_rank] + abc[:tensor_rank]
    B = ABC[0:multipole_additional_rank] + abc[tensor_rank - multipole_rank:tensor_rank]
    C = ABC[multipole_additional_rank:multipole_additional_rank + Rab_additional_rank] + abc[:field_rank]
    signature = "{},{}->{}".format(A, B, C)
    res = factor * np.einsum(signature, Tn, multipoles)
    return res


def thole_exp_field(Rab, multipole_rank, multipoles, field_rank, damping_factors):
    """
    Calculates the field (derivatives) due to a set of multipoles in a set of points
    Uses thole damping

    Rab: distance ndarrray
        shape is (nmult,...,3) so we can work with both a single distance vector (nmult,3), a vector of distance vectors (nmult,N,3) and so on
    multipole_rank: rank of the multipole
    multipoles: ndarray, cartesian multipole moments of the same rank, shape (number_of_multipoles) + (3)*multipole_rank
        0-> (number_of_multipoles,)    charge
        1-> (number_of_multipoles,3)   dipole
        2-> (number_of_multipoles,3,3) quadrupole
        ...
    field_rank: rank of output field
        0->  potential (negative of, depending on definitions)
        1->  field
        2->  field gradient
        ...
    """
    tensor_rank = field_rank + multipole_rank
    multipole_additional_rank = len(multipoles.shape) - multipole_rank
    Rab_additional_rank = len(Rab.shape) - multipole_additional_rank - 1

    Tn = tensors.T_damp_thole[tensor_rank](Rab, damping_factors)
    factor = -1.0 * (-1)**multipole_rank / factorial(multipole_rank)
    # Tn:multipoles->out
    # abc... for multipole/field contraction
    # ABC... for additional ranks
    A = ABC[0:multipole_additional_rank + Rab_additional_rank] + abc[:tensor_rank]
    B = ABC[0:multipole_additional_rank] + abc[tensor_rank - multipole_rank:tensor_rank]
    C = ABC[multipole_additional_rank:multipole_additional_rank + Rab_additional_rank] + abc[:field_rank]
    signature = "{},{}->{}".format(A, B, C)
    res = factor * np.einsum(signature, Tn, multipoles)
    return res


def thole_amoeba_field(Rab, multipole_rank, multipoles, field_rank, damping_factors):
    """
    Calculates the field (derivatives) due to a set of multipoles in a set of points
    Uses thole damping

    Rab: distance ndarrray
        shape is (nmult,...,3) so we can work with both a single distance vector (nmult,3), a vector of distance vectors (nmult,N,3) and so on
    multipole_rank: rank of the multipole
    multipoles: ndarray, cartesian multipole moments of the same rank, shape (number_of_multipoles) + (3)*multipole_rank
        0-> (number_of_multipoles,)    charge
        1-> (number_of_multipoles,3)   dipole
        2-> (number_of_multipoles,3,3) quadrupole
        ...
    field_rank: rank of output field
        0->  potential (negative of, depending on definitions)
        1->  field
        2->  field gradient
        ...
    """
    tensor_rank = field_rank + multipole_rank
    multipole_additional_rank = len(multipoles.shape) - multipole_rank
    Rab_additional_rank = len(Rab.shape) - multipole_additional_rank - 1

    Tn = tensors.T_damp_thole_amoeba[tensor_rank](Rab, damping_factors)
    factor = -1.0 * (-1)**multipole_rank / factorial(multipole_rank)
    # Tn:multipoles->out
    # abc... for multipole/field contraction
    # ABC... for additional ranks
    A = ABC[0:multipole_additional_rank + Rab_additional_rank] + abc[:tensor_rank]
    B = ABC[0:multipole_additional_rank] + abc[tensor_rank - multipole_rank:tensor_rank]
    C = ABC[multipole_additional_rank:multipole_additional_rank + Rab_additional_rank] + abc[:field_rank]
    signature = "{},{}->{}".format(A, B, C)
    res = factor * np.einsum(signature, Tn, multipoles)
    return res

def energy(system):
    exclusion_lists_permanent = [atom.excluded_interactions_permanent for atom in system.atoms]
    active_field_ranks = range(system.max_permanent_multipole_rank + 1)
    energy = [0.0]*len(active_field_ranks)
    # get potentials, fields
    for atom_i in system.atoms:
        atom_i.permanent_fields *= 0.0
        mask = np.ones(system.natoms, dtype=bool)
        for exclusion in exclusion_lists_permanent[atom_i.idx]:
            mask[exclusion] = False
        Rab = system.coordinates[atom_i.idx, :] - system.coordinates[mask, :]
        for field_rank in active_field_ranks:
            for multipole_rank in range(system.max_permanent_multipole_rank + 1):
                atom_i.permanent_fields[field_rank] += field(Rab, multipole_rank, system.permanent_moments[multipole_rank][mask], field_rank)
        # contract charge with potential; dipole with field, etc.
        for field_rank in active_field_ranks:
            energy[field_rank] += (-0.5) /  factorial(field_rank) * np.sum(atom_i.permanent_fields[field_rank] * atom_i.permanent_moments[field_rank])
    return sum(energy), energy
