#!/usr/bin/env python
import copy
import numpy as np
from . import tensors, fields


def iterative_solver(system, tol=1e-10, maxiter=1000, scheme="GS/DIIS", verbose=False, damp_induced="", damp_permanent=""):
    """
    Iterates over the system to converge induced moments. Fields (or derivatives) are set up from:
        1) permanent moments
        2) induced   moments
    These fields in turn create new induced moments through the relevant polarizabilities.
    The code is structured as follows:
        1) Indexing array is populated
        2) Permanent fields are calculated once and for all
        3) Single iteration of induced moments are calculated
        4) Main iteration loop, consisting of
            a) Obtaining fields from current induced moments
            b) Getting new induced moments from permanent + previous induced fields
    Remark: Fields and polarizabilities are stored on each atom, but induced/permanent) moments are stored in system
    """

    if len(system.active_field_ranks) == 0:
        return system
    iterations = 0
    # use this variable to check if we are converged in the moments. Initial value is just some larger-than-tol value
    sum_squared_error = tol + 1

    # setup for special methods
    if scheme == "JI/DIIS" or scheme == "GS/DIIS":
        DIIS_maxvecs = 100
        DIIS_previous_moments = []
        DIIS_errors = []

    exclusion_lists_permanent = [atom.excluded_interactions_permanent for atom in system.atoms]
    exclusion_lists_induced = [atom.excluded_interactions_induced for atom in system.atoms]

    # Obtain permanent fields
    for atom_i in system.atoms:
        # check sign
        atom_i.permanent_fields *= 0.0
        mask = np.ones(system.natoms, dtype=bool)
        for exclusion in exclusion_lists_permanent[atom_i.idx]:
            mask[exclusion] = False
        Rab = system.coordinates[atom_i.idx, :] - system.coordinates[mask, :]
        for field_rank in atom_i.active_field_ranks:
            # add external field
            # external field is list of [potential, field, field_gradient, ...] on each atom
            if system.external_field and len(system.external_field) >= field_rank and np.any(system.external_field[field_rank]):
                atom_i.permanent_fields[field_rank] += system.external_field[field_rank][atom_i.idx]
            # add permanent fields, possibly damped
            if damp_permanent == "thole-amoeba":
                for multipole_rank in range(system.max_permanent_multipole_rank + 1):
                    atom_i.permanent_fields[field_rank] += fields.thole_amoeba_field(Rab, multipole_rank, system.permanent_moments[multipole_rank][mask], field_rank,
                                                                                     system.damping_factors[atom_i.idx][mask])
            elif damp_permanent == "thole-exp":
                for multipole_rank in range(system.max_permanent_multipole_rank + 1):
                    atom_i.permanent_fields[field_rank] += fields.thole_exp_field(Rab, multipole_rank, system.permanent_moments[multipole_rank][mask], field_rank,
                                                                                  system.damping_factors[atom_i.idx][mask])
            else:
                for multipole_rank in range(system.max_permanent_multipole_rank + 1):
                    atom_i.permanent_fields[field_rank] += fields.field(Rab, multipole_rank, system.permanent_moments[multipole_rank][mask], field_rank)

    # Get "single-iteration" induced moments
    # Accumulate induced moments for each atom
    # If existing induced moments are present, use these as a starting guess instead
    if not np.any(np.abs(system.induced_moments[1]) > 0):
        for atom_i in system.atoms:
            for pol in atom_i.polarizabilities:
                source_atom = system.atoms[pol.field_source_idx]
                system.induced_moments[pol.moment_rank][atom_i.idx] += np.einsum(pol.signature, pol.values, *source_atom.permanent_fields[pol.field_ranks])
                # ^support for DistProp polarizability

    # Monitor changes in induced moments for convergence
    old_moments = copy.deepcopy(system.induced_moments)

    while iterations < maxiter and sum_squared_error >= tol:
        # 1) Get fields from induced moments
        if scheme == "GS" or scheme == "GS/DIIS":
            induced_moments = system.induced_moments
        elif scheme == "JI" or scheme == "JI/DIIS":
            induced_moments = old_moments
        else:
            raise NotImplementedError
        # main update loop
        for atom_i in system.atoms:
            mask = np.ones(system.natoms, dtype=bool)
            for exclusion in exclusion_lists_induced[atom_i.idx]:
                mask[exclusion] = False
            Rab = system.coordinates[atom_i.idx, :] - system.coordinates[mask, :]
            for field_rank in atom_i.active_field_ranks:
                # reset old fields first
                atom_i.induced_fields[field_rank] *= 0.0
                # and get field from induced
                for multipole_rank in system.active_induced_multipole_ranks:
                    if damp_induced:
                        if damp_induced == "thole-exp":
                            atom_i.induced_fields[field_rank] += fields.thole_exp_field(Rab, multipole_rank, induced_moments[multipole_rank][mask], field_rank,
                                                                                        system.damping_factors[atom_i.idx][mask])
                        elif damp_induced == "thole-amoeba":
                            atom_i.induced_fields[field_rank] += fields.thole_amoeba_field(Rab, multipole_rank, induced_moments[multipole_rank][mask], field_rank,
                                                                                           system.damping_factors[atom_i.idx][mask])
                        else:
                            raise NotImplementedError(f"Option {damp_induced} does not exist")
                    else:
                        atom_i.induced_fields[field_rank] += fields.field(Rab, multipole_rank, induced_moments[multipole_rank][mask], field_rank)
            # 2) update induced moments -
            # 2a) Reset old induced moments
            for pol in atom_i.polarizabilities:
                system.induced_moments[pol.moment_rank][atom_i.idx] *= 0.0
            # 2b) Accumulate new induced moment
            for pol in atom_i.polarizabilities:
                source_atom = system.atoms[pol.field_source_idx]
                field = source_atom.induced_fields[pol.field_ranks] + source_atom.permanent_fields[pol.field_ranks]
                system.induced_moments[pol.moment_rank][atom_i.idx] += np.einsum(pol.signature, pol.values, *field)

        # 3) Check for convergence
        previous_sum_squared_error = sum_squared_error
        sum_squared_error = 0.0

        for imom in range(len(system.induced_moments)):
            for i in range(len(system.induced_moments[imom])):
                sum_squared_error += np.linalg.norm(system.induced_moments[imom][i] - old_moments[imom][i])

        # 3b) for DIIS methods, compute DIIS guess
        if scheme == "GS/DIIS" or scheme == "JI/DIIS":
            DIIS_previous_moments.append(copy.deepcopy(system.induced_moments))
            if len(DIIS_previous_moments) > DIIS_maxvecs:
                DIIS_previous_moments.pop(0)
            DIIS_errors.append(system.induced_moments[1] - old_moments[1])
            if len(DIIS_errors) > DIIS_maxvecs:
                DIIS_errors.pop(0)

            # construct DIIS matrix
            if len(DIIS_errors) > 2:
                DIIS_size = len(DIIS_errors) + 1
                bmat = np.zeros((DIIS_size, DIIS_size))
                for i in range(1, DIIS_size):
                    bmat[i, 0] = bmat[0, i] = -1.0
                for i in range(1, DIIS_size):
                    for j in range(1, DIIS_size):
                        bmat[i, j] = bmat[j, i] = np.dot(DIIS_errors[i - 1].flat, DIIS_errors[j - 1].flat)
                rhs = np.zeros(DIIS_size)
                rhs[0] = -1.0
                weights = np.linalg.solve(bmat, rhs)[1:]  # first weight is the lagrange multiplier
                system.induced_moments[1] *= 0.0
                for i in range(DIIS_size - 1):
                    system.induced_moments[1] += weights[i] * DIIS_previous_moments[i][1]

        old_moments = copy.deepcopy(system.induced_moments)
        ratio = previous_sum_squared_error / sum_squared_error
        iterations += 1
        if verbose:
            print("Sum-squared-error in dipoles is {}. Ratio is {}".format(sum_squared_error, ratio))
            print(np.sum(system.induced_moments[1], axis=0))
    if verbose:
        print("{}".format(iterations), end=" ")
    return system


def get_permanent_field(system):
    exclusion_lists_permanent = [atom.excluded_interactions_permanent for atom in system.atoms]
    for atom_i in system.atoms:
        mask = np.ones(system.natoms, dtype=bool)
        for exclusion in exclusion_lists_permanent[atom_i.idx]:
            mask[exclusion] = False
        Rab = system.coordinates[atom_i.idx, :] - system.coordinates[mask, :]
        atom_i.permanent_fields *= 0.0
        for field_rank in atom_i.active_field_ranks:
            for multipole_rank in range(system.max_permanent_multipole_rank + 1):
                atom_i.permanent_fields[field_rank] += fields.field(Rab, multipole_rank, system.permanent_moments[multipole_rank][mask], field_rank)
    field = np.zeros((len(system.atoms), 3))
    for atom_i in system.atoms:
        field[atom_i.idx, :] = atom_i.permanent_fields[1]
    return field
