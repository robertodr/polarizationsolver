#!/usr/bin/env python

import functools
import periodictable

def potwriter(filename, data):
    with open(filename, 'w') as f:
        f.write('@COORDINATES\n')
        f.write(f'{len(data["coordinates"])}\n')
        f.write('AA\n')
        for i, (element, coord) in enumerate(zip(data['elements'], data['coordinates'] / 1.88972598858)):
            f.write('{:6s} {:16.12f} {:16.12f} {:16.12f} {:12d}\n'.format(element, *coord, i+1))
        if 'moments' in data.keys():
            f.write('@MULTIPOLES\n')
            for order, multipoles in data['moments'].items():
                f.write(f'ORDER {order}\n')
                f.write(f'{len(multipoles)}\n')
                fmt = '{:<12d} ' + '{:16.12f} ' * ((order+1)*(order+2)//2) + '\n'
                for i, multipole in enumerate(multipoles):
                    f.write(fmt.format(i+1, *multipole))
        if 'polarizabilities' in data.keys():
            f.write('@POLARIZABILITIES\n')
            for order, polarizabilities in data['polarizabilities'].items():
                f.write(f'ORDER {" ".join(map(str, order))}\n')
                f.write(f'{len(polarizabilities)}\n')
                order = sum(order)
                fmt = '{:<12d} ' + '{:16.12f} ' * ((order+1)*(order+2)//2) + '\n'
                for i, polarizability in enumerate(polarizabilities):
                    f.write(fmt.format(i+1, *polarizability))
        if 'exclusions' in data.keys():
            f.write('EXCLISTS\n')
            maxlength = max(map(len, data['exclusions'].values()))
            f.write(f'{len(data["exclusions"])} {maxlength}\n')
            fmt = '{:<12d} ' + '{:>12d} ' * (maxlength - 1) + '\n'
            for excl in data['exclusions'].values():
                f.write(fmt.format(*map(lambda x: x+1, excl)))

def molwriter(filename, elements, coordinates, basis='cc-pVDZ', charge=0):
    coordinates = coordinates / 1.88972598858
    atomtypes = []
    element = elements[0]
    last_element = element
    group_coordinates = []
    group_coordinates.append(coordinates[0])
    group = (element, group_coordinates)
    for element, coordinate in zip(elements[1:], coordinates[1:]):
        if element == last_element:
            group_coordinates.append(coordinate)
        else:
            atomtypes.append(group)
            last_element = element
            group_coordinates = []
            group = (element, group_coordinates)
            group_coordinates.append(coordinate)
    atomtypes.append(group)
    with open(filename, 'w') as f:
        f.write(f'BASIS\n{basis}\n\n\n')
        f.write(f'Atomtypes={len(atomtypes)} Charge={charge} Angstrom NoSymmetry\n')
        for (element, coordinates) in atomtypes:
            charge = periodictable.__dict__[element].number
            f.write(f'Atoms={len(coordinates)} Charge={charge}\n')
            for coordinate in coordinates:
                f.write(f'{element}  {coordinate[0]:16.12f}  {coordinate[1]:16.12f}  {coordinate[2]:16.12f}\n')
