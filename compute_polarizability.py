#!/usr/bin/env python

from polarizationsolver import readers
from polarizationsolver import solvers
from polarizationsolver import fields
import argparse
import numpy as np
import copy

parser = argparse.ArgumentParser(
    description="Calculate electrostatic potential in specified points")
parser.add_argument("-i",
                    "--pot",
                    dest="pot",
                    type=str,
                    required=True,
                    help="Location of potential file")
parser.add_argument("--solver",
                    dest="solver",
                    type=str,
                    default="GS/DIIS",
                    choices=["GS/DIIS", "JI/DIIS", "GS", "Jacobi"],
                    help="Which induced moment solver to use")
parser.add_argument("--tolerance",
                    dest="tolerance",
                    type=float,
                    default=1e-10,
                    help="Tolerance in induced moment solver")
parser.add_argument("--damp-induced",
                    dest="damp",
                    type=str,
                    default="",
                    help="Enable damping of induced moments",
                    choices=["", "thole-amoeba", "thole-exp"])
parser.add_argument("--verbose",
                    dest="verbose",
                    default=False,
                    action="store_true",
                    help="Print out more information")
parser.add_argument("--finite-field", dest="field", type=float, default=1e-3)

# load in pot file and solve for induced moments
args = parser.parse_args()
data = readers.potreader(args.pot)
system = readers.parser(data)
solvers.iterative_solver(system,
                         scheme=args.solver,
                         tol=args.tolerance,
                         damp_induced=args.damp,
                         verbose=args.verbose)

polarizability = np.zeros((3, 3))
for direction in [0, 1, 2]:
    external_field = np.zeros(3)
    external_field[direction] = args.field
    system.external_field = [0., [external_field] * system.natoms]
    solvers.iterative_solver(system,
                             scheme=args.solver,
                             tol=args.tolerance,
                             damp_induced=args.damp,
                             verbose=args.verbose)
    plus = copy.deepcopy(system.induced_moments[1])
    external_field[direction] = -args.field
    system.external_field = [0., [external_field] * system.natoms]
    solvers.iterative_solver(system,
                             scheme=args.solver,
                             tol=args.tolerance,
                             damp_induced=args.damp,
                             verbose=args.verbose)
    minus = copy.deepcopy(system.induced_moments[1])
    polarizability[:, direction] = np.sum((plus - minus) / (2 * args.field),
                                          axis=0)

print('The molecular polarizability is')
print(polarizability)
print()
print(f'(isotropic): {np.trace(polarizability)/3}')
