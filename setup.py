#!/usr/bin/env python
from distutils.core import setup

setup(name="polarizationsolver",
      version="0.0.0.dev0",
      packages=["polarizationsolver"],
      license="GNU General Public License v3.0",
      long_description="Python program to solve induced multipole problems")
