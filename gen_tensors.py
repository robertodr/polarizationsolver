#!/usr/bin/env python
import sympy
import sys
import numpy as np
from functools import wraps

maxorder = int(sys.argv[1])
maxorder_damped = int(sys.argv[2])

def symmetry(function):
    cache = {}

    @wraps(function)
    def wrapper(arg):
        arg = ''.join(sorted(arg))
        if arg in cache:
            return cache[arg]
        else:
            val = function(arg)
            cache[arg] = val
            return val

    return wrapper


@symmetry
def T_cart(sequence):
    """
    T = 1/R
    Generates sympy cartesian T-tensor of rank len(sequence)
    """
    x, y, z = sympy.symbols("x y z")
    R = sympy.sqrt(x**2 + y**2 + z**2)
    T = 1 / R
    if sequence:
        return sympy.diff(T, *[i for i in sequence])
    else:
        return T


@symmetry
def T_damp_erf(sequence):
    """
    S = v*r = f(r)
    f(r) = r/(erf(sqrt(a)*r))
    T = 1/S
    """
    x, y, z, a = sympy.symbols("x y z a")
    R = sympy.sqrt(x**2 + y**2 + z**2)
    S = R / sympy.erf(sympy.sqrt(a) * R)
    T = 1 / S
    if sequence:
        return sympy.diff(T, *[i for i in sequence])
    else:
        return T


@symmetry
def T_damp_thole(sequence):
    """
    Swart, M., and P. Th van Duijnen. "DRF90: a polarizable force field." Molecular Simulation 32.6 (2006): 471-484.
    u = r/(a_i*a_j)**(1/6) 
    a_i: polarizability on atom i
    a_j: polarizability on atom j
    v = damp*u
    damp: screening factor
    v = damp*u = r/(a_i*a_j)**(1/6)*damp = r*a
    a = 1/(a_i*a_j)**(1/6)*damp
    """
    x, y, z, a = sympy.symbols("x y z a")
    R = sympy.sqrt(x**2 + y**2 + z**2)
    v = R * a
    fV = 1 - (sympy.Rational(1, 2) * v + 1) * sympy.exp(-v)
    T = fV / R
    if sequence:
        return sympy.diff(T, *[i for i in sequence])
    else:
        return T


@symmetry
def T_damp_thole_amoeba(sequence):
    x, y, z, a = sympy.symbols("x y z a")
    """
    From:
    The polarizable point dipoles method withelectrostatic damping: Implementation on amodel system
     J. Chem. Phys. 133, 234101 (2010);

    AMOEBA: exp(-au**3)  <- exp(-(ra)**3) =>
    a = 1/(alpha_i * alpha_j)**1/6 * (damp)**1/3.  
    """
    R = sympy.sqrt(x**2 + y**2 + z**2)
    v = a * R
    s0 = 1 - sympy.exp(-(v)**3) + v * sympy.functions.special.gamma_functions.uppergamma(
        sympy.Rational(2, 3), (v)**3)
    s1 = 1 - sympy.exp(-(v)**3)
    T = s0 / R
    Tx = -s1 * x / R**3
    Ty = -s1 * y / R**3
    Tz = -s1 * z / R**3
    if sequence:
        if len(sequence) > 1:
            if sequence[0] == "x":
                return sympy.diff(Tx, *[i for i in sequence[1:]])
            elif sequence[0] == "y":
                return sympy.diff(Ty, *[i for i in sequence[1:]])
            elif sequence[0] == "z":
                return sympy.diff(Tz, *[i for i in sequence[1:]])
        else:
            if sequence[0] == "x":
                return Tx
            elif sequence[0] == "y":
                return Ty
            elif sequence[0] == "z":
                return Tz
    else:
        return T


seqs = [""]
seq2int = {"x": "0", "y": "1", "z": "2"}
print("import numpy as np")
print("from numpy import sqrt, exp")
print("from scipy.special import erf")
print("from scipy.special import gammainc, gamma")
print("def uppergamma(s, x): return gamma(s)*(1 - gammainc(s,x))")

# cartesian
for order in range(maxorder + 1):
    # jit here?
    print(f"def T{order}(Rab):")
    print(f"    x = Rab[...,0]")
    print(f"    y = Rab[...,1]")
    print(f"    z = Rab[...,2]")
    print(f"    shape = Rab.shape[:-1] + (3,)*{order}")
    print(f"    result = np.zeros(shape, dtype=np.float64)")
    #components = [T_cart(seq).simplify() for seq in seqs]
    components = [T_cart(seq) for seq in seqs]
    cse = sympy.cse(components, optimizations="basic")
    cse_intermediates = cse[0]
    cse_outputs = cse[1]

    for intermediate in cse_intermediates:
        print(f"    {intermediate[0]} = {intermediate[1]}")
    for i, seq in enumerate(seqs):
        print(
            f"    result[...,{','.join([seq2int[s] for s in seq])}] = {cse_outputs[i]}"
        )
    print(f"    return result")

    newseq = []
    for s in seqs:
        newseq.append(s + "x")
        newseq.append(s + "y")
        newseq.append(s + "z")
    seqs = newseq
print(f'T = [{",".join(["T" + str(i) for i in range(maxorder+1)])}]')

# erf-damped
# seqs = ['']
# for order in range(maxorder+1):
#    print(f'def Terf{order}(Rab, a):')
#    print(f'    x = Rab[...,0]')
#    print(f'    y = Rab[...,1]')
#    print(f'    z = Rab[...,2]')
#    print(f'    shape = Rab.shape[:-1] + (3,)*{order}')
#    print(f'    result = np.zeros(shape, dtype=np.float64)')
#    components = [T_damp_erf(seq) for seq in seqs]
#    cse = sympy.cse(components, optimizations='basic')
#    cse_intermediates = cse[0]
#    cse_outputs = cse[1]
#
#    for intermediate in cse_intermediates:
#        print(f'    {intermediate[0]} = {intermediate[1]}')
#    for i, seq in enumerate(seqs):
#        print(f"    result[...,{','.join([seq2int[s] for s in seq])}] = {cse_outputs[i]}")
#    print(f'    return result')
#
#    newseq = []
#    for s in seqs:
#        newseq.append(s + 'x')
#        newseq.append(s + 'y')
#        newseq.append(s + 'z')
#    seqs = newseq
# print(f'Terf = [{",".join(["Terf" + str(i) for i in range(maxorder+1)])}]')

# thole-damped
seqs = [""]
for order in range(maxorder_damped + 1):
    print(f"def T_damp_thole_{order}(Rab, a):")
    print(f"    x = Rab[...,0]")
    print(f"    y = Rab[...,1]")
    print(f"    z = Rab[...,2]")
    print(f"    shape = Rab.shape[:-1] + (3,)*{order}")
    print(f"    result = np.zeros(shape, dtype=np.float64)")
    components = [T_damp_thole(seq) for seq in seqs]
    cse = sympy.cse(components, optimizations="basic")
    cse_intermediates = cse[0]
    cse_outputs = cse[1]

    for intermediate in cse_intermediates:
        print(f"    {intermediate[0]} = {intermediate[1]}")
    for i, seq in enumerate(seqs):
        print(
            f"    result[...,{','.join([seq2int[s] for s in seq])}] = {cse_outputs[i]}"
        )
    print(f"    return result")

    newseq = []
    for s in seqs:
        newseq.append(s + "x")
        newseq.append(s + "y")
        newseq.append(s + "z")
    seqs = newseq
print(
    f'T_damp_thole = [{",".join(["T_damp_thole_" + str(i) for i in range(maxorder_damped+1)])}]'
)

# amoeba-damped
seqs = [""]
for order in range(maxorder_damped + 1):
    print(f"def T_damp_thole_amoeba_{order}(Rab, a):")
    print(f"    x = Rab[...,0]")
    print(f"    y = Rab[...,1]")
    print(f"    z = Rab[...,2]")
    print(f"    shape = Rab.shape[:-1] + (3,)*{order}")
    print(f"    result = np.zeros(shape, dtype=np.float64)")
    components = [T_damp_thole_amoeba(seq) for seq in seqs]
    cse = sympy.cse(components, optimizations="basic")
    cse_intermediates = cse[0]
    cse_outputs = cse[1]

    for intermediate in cse_intermediates:
        print(f"    {intermediate[0]} = {intermediate[1]}")
    for i, seq in enumerate(seqs):
        print(
            f"    result[...,{','.join([seq2int[s] for s in seq])}] = {cse_outputs[i]}"
        )
    print(f"    return result")

    newseq = []
    for s in seqs:
        newseq.append(s + "x")
        newseq.append(s + "y")
        newseq.append(s + "z")
    seqs = newseq
print(
    f'T_damp_thole_amoeba = [{",".join(["T_damp_thole_amoeba_" + str(i) for i in range(maxorder_damped+1)])}]'
)
