#!/usr/bin/env python


def test_2h2o():
    import numpy as np
    from polarizationsolver.universe import system, atom, polarizability
    from polarizationsolver.readers import potreader, parser
    from polarizationsolver.solvers import iterative_solver
    import cProfile
    import sys
    import time
    import copy

    data = potreader("2h2o.pot")
    u = parser(data)
    iterative_solver(u)
    iterative_solver(u)

    reference = np.array([[-1.45734854e-03, -2.25509005e-03, -5.37119471e-06],
                          [-1.70798918e-03, -1.20166624e-03, -1.63670480e-06],
                          [-5.16249646e-04, -1.85432247e-04, -1.04094726e-06],
                          [-6.78253723e-03, -4.75557872e-03, -6.82657406e-06],
                          [-8.33331689e-04, 2.30680309e-04, -1.74411782e-06],
                          [2.70828873e-03, -3.39660387e-04, -4.40030766e-06],
                          [1.96061580e-03, 5.36556502e-04, -2.76482515e-07],
                          [4.71458709e-04, 2.52715018e-04, 1.15791452e-06],
                          [7.32247310e-03, 2.28999655e-03, -2.03349024e-06],
                          [5.73753790e-04, 5.67239558e-04, 3.74666797e-06]])

    assert np.allclose(reference, u.induced_moments[1])
    iterative_solver(u, fast_field='BH')
    assert np.allclose(reference, u.induced_moments[1])
    
