#!/usr/bin/env python
import sympy
import sys
import numpy as np
from functools import wraps
from collections import defaultdict
import re

maxorder = int(sys.argv[1])
min_uses = int(sys.argv[2])


def symmetry(function):
    cache = {}

    @wraps(function)
    def wrapper(arg):
        arg = ''.join(sorted(arg))
        if arg in cache:
            return cache[arg]
        else:
            val = function(arg)
            cache[arg] = val
            return val

    return wrapper


@symmetry
def T_cart(sequence):
    """
    T = 1/R
    Generates sympy cartesian T-tensor of rank len(sequence)
    """
    x, y, z = sympy.symbols("x y z")
    R = sympy.sqrt(x**2 + y**2 + z**2)
    T = 1 / R
    if sequence:
        return sympy.diff(T, *[i for i in sequence])
    else:
        return T

@symmetry
def T_damp_erf(sequence):
    """
    S = v*r = f(r)
    f(r) = r/(erf(sqrt(a)*r))
    T = 1/S
    """
    x, y, z, a = sympy.symbols("x y z a")
    R = sympy.sqrt(x**2 + y**2 + z**2)
    S = R / sympy.erf(sympy.sqrt(a) * R)
    T = 1 / S
    if sequence:
        return sympy.diff(T, *[i for i in sequence])
    else:
        return T


@symmetry
def T_damp_thole(sequence):
    """
    Swart, M., and P. Th van Duijnen. "DRF90: a polarizable force field." Molecular Simulation 32.6 (2006): 471-484.
    u = r/(a_i*a_j)**(1/6) 
    a_i: polarizability on atom i
    a_j: polarizability on atom j
    v = damp*u
    damp: screening factor
    v = damp*u = r/(a_i*a_j)**(1/6)*damp = r*a
    a = 1/(a_i*a_j)**(1/6)*damp
    """
    x, y, z, a = sympy.symbols("x y z a")
    R = sympy.sqrt(x**2 + y**2 + z**2)
    v = R * a
    fV = 1 - (sympy.Rational(1, 2) * v + 1) * sympy.exp(-v)
    T = fV / R
    if sequence:
        return sympy.diff(T, *[i for i in sequence])
    else:
        return T


@symmetry
def T_damp_thole_amoeba(sequence):
    x, y, z, a = sympy.symbols("x y z a")
    """
    From:
    The polarizable point dipoles method withelectrostatic damping: Implementation on amodel system
     J. Chem. Phys. 133, 234101 (2010);

    AMOEBA: exp(-au**3)  <- exp(-(ra)**3) =>
    a = 1/(alpha_i * alpha_j)**1/6 * (damp)**1/3.  
    """
    R = sympy.sqrt(x**2 + y**2 + z**2)
    v = a * R
    s0 = 1 - sympy.exp(-(v)**3) + v * sympy.functions.special.gamma_functions.uppergamma(
        sympy.Rational(2, 3), (v)**3)
    s1 = 1 - sympy.exp(-(v)**3)
    T = s0 / R
    Tx = -s1 * x / R**3
    Ty = -s1 * y / R**3
    Tz = -s1 * z / R**3
    if sequence:
        if len(sequence) > 1:
            if sequence[0] == "x":
                return sympy.diff(Tx, *[i for i in sequence[1:]])
            elif sequence[0] == "y":
                return sympy.diff(Ty, *[i for i in sequence[1:]])
            elif sequence[0] == "z":
                return sympy.diff(Tz, *[i for i in sequence[1:]])
        else:
            if sequence[0] == "x":
                return Tx
            elif sequence[0] == "y":
                return Ty
            elif sequence[0] == "z":
                return Tz
    else:
        return sympy.sympify('0') #T just skip it


tensor_types = {
        "": T_cart,
        "_damp_erf": T_damp_erf,
        "_damp_thole": T_damp_thole,
        "_damp_amoeba": T_damp_thole_amoeba,
}

for suffix, tensor_type in tensor_types.items():
    maxorder = int(sys.argv[1])
    if suffix:
        maxorder = min(maxorder, 6)

    seqs = [""]
    seq2int = {"x": "0", "y": "1", "z": "2"}

    print(f"""\
    module T_tensor{suffix}
       implicit none
       private
    """)
    print(f"   public Tn{suffix}")
    for order in range(maxorder + 1):
        print(f"   public T{order}{suffix}")
    print( "   contains")
    if suffix:
        print(f"   pure subroutine Tn{suffix}(n, x, y, z, a, T)")
    else:
        print(f"   pure subroutine Tn{suffix}(n, x, y, z, T)")

    print( "       integer, intent(in) :: n")
    print( "       real*8, intent(in) :: x(:), y(size(x)), z(size(x))")
    if suffix:
        print( "       real*8, intent(in) :: a(:)")
    print(f"       real*8, intent(inout) :: T(size(x), (n+1)*(n+2)*(n+3)/6)")
    print( "       select case (n)")
    for order in range(maxorder + 1):
        print(f"         case ({order})")
        for n in range(order+1):
            start = 1+(n)*(n+1)*(n+2)//6
            stop = (n+1)*(n+2)*(n+3)//6
            if suffix:
                print(f"             call T{n}{suffix}(x, y, z, a, T(:, {start}:{stop}))")
            else:
                print(f"             call T{n}{suffix}(x, y, z, T(:, {start}:{stop}))")

    print(f"         case default")
    for n in range(maxorder+1):
        start = 1+(n)*(n+1)*(n+2)//6
        stop = (n+1)*(n+2)*(n+3)//6
        if suffix:
            print(f"             call T{n}{suffix}(x, y, z, a, T(:, {start}:{stop}))")
        else:
            print(f"             call T{n}{suffix}(x, y, z, T(:, {start}:{stop}))")

    #print(f"             do k={maxorder+1}, n")
    #print(f"                 call T_recursive(k, x, y, z, T(:, 1+(k)*(k+1)*(k+2)/6:(k+1)*(k+2)*(k+3)/6))")
    #print(f"             end do")
    print("       end select")
    print(f"   end subroutine Tn{suffix}")



    components = []
    for order in range(maxorder + 1):
        components = [tensor_type(seq) for seq in seqs]
        newseq = []
        for s in seqs:
            newseq.append(''.join(sorted(s + "x")))
            newseq.append(''.join(sorted(s + "y")))
            newseq.append(''.join(sorted(s + "z")))
        seqs = sorted(list(set(newseq)))

        cse = sympy.cse(components, optimizations="basic")
        cse_intermediates = cse[0]
        cse_outputs = cse[1]

        uses = defaultdict(int)
        for symbol in cse_intermediates:
            for expr in cse_intermediates:
                expr = expr[1]
                if symbol[0] in expr.free_symbols:
                    uses[symbol] += 1
            for expr in cse_outputs:
                if symbol[0] in expr.free_symbols:
                    uses[symbol] += 1
        for symbol, count in reversed(list(uses.items())):
            if count < min_uses:
                for i in range(len(cse_outputs)):
                    cse_outputs[i] = cse_outputs[i].subs(symbol[0], symbol[1])
                for i in range(len(cse_intermediates)):
                    if cse_intermediates[i][0] is None:
                        continue
                    elif symbol[0] == cse_intermediates[i][0]:
                        cse_intermediates[i] = (None, None)
                    else:
                        interm = cse_intermediates[i]
                        cse_intermediates[i] = (interm[0], interm[1].subs(symbol[0], symbol[1]))


        if suffix:
            print(f"pure subroutine T{order}{suffix} (x, y, z, a, T)")
        else:
            print(f"pure subroutine T{order}{suffix} (x, y, z, T)")
        print( "    real*8, intent(in) :: x(:), y(size(x)), z(size(x))")
        if suffix:
            print( "    real*8, intent(in) :: a(:)")
        print(f"    real*8, intent(inout) :: T(size(x), {(order+1)*(order+2)*(order+3)//6})")
        print( "    integer :: i")
        print( "    real*8 :: xi, yi, zi")
        if suffix:
            print( "    real*8 :: ai")
        for intermediate in cse_intermediates:
            if intermediate[0] is not None:
                print(f"    real*8 :: {intermediate[0]}")
        # do
        print(f"    do i=1, size(x)")
        print("xi = x(i)")
        print("yi = y(i)")
        print("zi = z(i)")
        if suffix:
            print("ai = a(i)")
        for intermediate in cse_intermediates:
            if intermediate[0] is not None:
                print(re.sub('&\n +', '', f"        {intermediate[0].evalf()} = {sympy.fcode(intermediate[1].evalf().subs('x', 'xi').subs('y', 'yi').subs('z', 'zi').subs('a', 'ai'), source_format='free', standard=2008)}"))
        for i, output in enumerate(cse_outputs):
           print(re.sub("&\n +", "", f"        T(i, {i+1}) = {sympy.fcode(output.evalf().subs('x', 'xi').subs('y', 'yi').subs('z', 'zi').subs('a', 'ai').evalf(), source_format='free', standard=2008)}"))
        print(f"    end do")
        #end do
        print(f"end subroutine T{order}{suffix}")
    print(f"end module T_tensor{suffix}")
